import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        int sum = 0;
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum = sum + arr[i];
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = " + sum);
        double avg = sum / arr.length;
        System.out.println("avg = " + avg);

        int min = 0;
        for (int i=1;i<arr.length;i++){
            if(arr[min]>arr[i]){
                min = i;
            }
        }
        System.out.print("min = "+arr[min]);
    }
}
