import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        int index = -1;
        System.out.print("please input search value: ");
        int fine = sc.nextInt();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == fine) {
                index = i;
                break;
            }

        }
        if (index >= 0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found");
        }
    }
}
